# websockify-tls

[websockify](https://github.com/novnc/websockify/blob/master/other/js/websockify.js) by Joel Martin
modified to work with docker, modernized and published to npm as an easy-to-use package.

## How to use

###1. Command
```bash
npm install -g websockify-tls
websockify [source_addr:]source_port target_addr:target_port 
```

###2. Docker
```bash
docker run -it 
```

### Options

- `[--web dir]` serves files statically under `dir` with automatic indexing
- `[--cert localhost.crt [--key localhost.key]]` serves over SSL, default use ./localhost.crt and localhost.key
- `[--ssl-only ]` disallow non-encrypted client connections
- `[--ssl-target ]` connect to SSL target as SSL client

## License

AGPL (per original license)
 