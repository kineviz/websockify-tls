#!/usr/bin/env node

// A WebSocket to TCP socket proxy

const argv = require("optimist").argv,
  net = require("net"),
  tls = require("tls"),
  http = require("http"),
  https = require("https"),
  fs = require("fs"),
  finalhandler = require('finalhandler'),
  serveStatic = require('serve-static'),
  Buffer = require("buffer").Buffer,
  WebSocketServer = require("ws").Server;


let webServer,
  wsServer,
  sourceHost,
  sourcePort,
  targetHost,
  targetPort;

function newTlsOptions(hostname, ca = undefined) {
  return {
    rejectUnauthorized: false, // we manually check for this in the connect callback, to give a more helpful error to the user
    servername: hostname, // server name for the SNI (Server Name Indication) TLS extension
    ca: ca, // optional CA useful for TRUST_CUSTOM_CA_SIGNED_CERTIFICATES trust mode
  };
}

function decodeBuffer(buf) {
  var returnString = '';
  for (var i = 0; i < buf.length; i++) {
    if (buf[i] >= 48 && buf[i] <= 90) {
      returnString += String.fromCharCode(buf[i]);
    } else if (buf[i] === 95) {
      returnString += String.fromCharCode(buf[i]);
    } else if (buf[i] >= 97 && buf[i] <= 122) {
      returnString += String.fromCharCode(buf[i]);
    } else {
      var charToConvert = buf[i].toString(16);
      if (charToConvert.length === 0) {
        returnString += '\\x00';
      } else if (charToConvert.length === 1) {
        returnString += '\\x0' + charToConvert;
      } else {
        returnString += '\\x' + charToConvert;
      }
    }
  }
  return returnString;
}

// Handle new WebSocket client
let handelWebsocketClient = function handelWebsocketClient(wsClient, req) {
  let clientAddr = wsClient._socket.remoteAddress;
  let log = function (...msgs) {
    console.log(` ${clientAddr}:`, msgs);
  };
  log("WebSocket connection");

  // let target = net.createConnection(targetPort, targetHost, function () {
  //   log("connected to target");
  // });

  const tlsOpts = newTlsOptions(targetHost);
  target = tls.connect(targetPort, targetHost, tlsOpts, function () {
    const certificate = target.getPeerCertificate();
    log("connected to target :", certificate);
  })

  target.setEncoding('utf8');
  target.setKeepAlive(true);

  target.on("data", function (data) {
    log("sending message: " + data);
    try {
      wsClient.send(data);
    } catch (e) {
      log("Client closed, cleaning up target :", e);
      target.end();
    }
  });
  target.on("end", function () {
    log("target disconnected");
    wsClient.close();
  });
  target.on("error", function (err) {
    log("target connection error :", err);
    target.end();
    wsClient.close();
  });

  wsClient.on("message", function (msg) {
    //log('got message: ' + msg);
    target.write(Buffer.from(msg));
  });
  wsClient.on("close", function (code, reason) {
    log("WebSocket client disconnected: " + code + " [" + reason + "]");
    target.end();
  });
  wsClient.on("error", function (a) {
    log("WebSocket client error: " + a);
    target.end();
  });
};

// Send an HTTP error response
let handleHttpError = function (response, code, msg) {
  response.writeHead(code, { "Content-Type": "text/plain" });
  response.write(msg + "\n");
  response.end();
  return;
};

// Process an HTTP static file request
let serve = argv.web ? serveStatic(argv.web) : null
let handleHttpRequest = function (req, res) {
  if (!serve) {
    return handleHttpError(res, 403, `403 A WebSocket to TCP socket proxy`);
  }
  serve(req, res, finalhandler(req, res))
};

// parse source and target arguments into parts
try {
  let sourceArg = argv._[0].toString();
  let targetArg = argv._[1].toString();

  let idx;
  idx = sourceArg.indexOf(":");
  if (idx >= 0) {
    sourceHost = sourceArg.slice(0, idx);
    sourcePort = parseInt(sourceArg.slice(idx + 1), 10);
  } else {
    sourceHost = "";
    sourcePort = parseInt(sourceArg, 10);
  }

  idx = targetArg.indexOf(":");
  if (idx < 0) {
    throw "target must be host:port";
  }
  targetHost = targetArg.slice(0, idx);
  targetPort = parseInt(targetArg.slice(idx + 1), 10);

  if (isNaN(sourcePort) || isNaN(targetPort)) {
    throw "illegal port";
  }
} catch (e) {
  console.error(
    "websockify [--web web_dir] [--cert cert.pem [--key key.pem]] [source_addr:]sourcePort target_addr:targetPort"
  );
  process.exit(2);
}

console.log("WebSocket settings: ");
console.log(
  "    - proxying from " +
  sourceHost +
  ":" +
  sourcePort +
  " to " +
  targetHost +
  ":" +
  targetPort
);
if (argv.web) {
  console.log("    - Web server active. Serving: " + argv.web);
}

if (argv.cert) {
  argv.key = argv.key || argv.cert;
  let cert = fs.readFileSync(argv.cert), key = fs.readFileSync(argv.key);
  console.log(
    "    - Running in encrypted HTTPS (wss://) mode using: " +
    argv.cert +
    ", " +
    argv.key
  );
  webServer = https.createServer({ cert: cert, key: key }, handleHttpRequest);
} else {
  console.log("    - Running in unencrypted HTTP (ws://) mode");
  webServer = http.createServer(handleHttpRequest);
}

wsServer = new WebSocketServer({ noServer:true });
wsServer.on("connection", handelWebsocketClient);

webServer.on('upgrade', function upgrade(request, socket, head) {
  wsServer.handleUpgrade(request, socket, head, function done(ws) {
    wsServer.emit('connection', ws, request);
  });
});

webServer.listen(sourcePort, sourceHost, () => {
  console.log(`Server running at  http${argv.cert ? "s" : ""}://${sourceHost}:${sourcePort}/`);
});